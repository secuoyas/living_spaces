import React from "react"
import { configure, addDecorator, addParameters } from "@storybook/react"
import { ThemeProvider } from "styled-components"
import { theme } from "../src/utils/themeGet"
import { parseTheme } from "@sqymagma/theme"

import _mainTheme from "../src/themes/siteTheme"

global.___loader = {
  enqueue: () => {},
  hovering: () => {},
}

global.__PATH_PREFIX__ = ""

window.___navigate = pathname => {
  action("NavigateTo:")(pathname)
}

addDecorator(story => <ThemeProvider theme={parseTheme(_mainTheme)}>{story()}</ThemeProvider>)

addParameters({
  options: {
    storySort: (a, b) => {
      return a[1].kind === b[1].kind ? 0 : a[1].id.localeCompare(b[1].id, { numeric: true })
    },
  },
})

// configure(load, module)
configure(require.context("../src/stories", true, /[\w\d\s]+\.story\.(js|mdx)$/), module)
