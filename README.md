![Logo](./assets/gatsby-magma-starter.png)

# Gatsby Magma starter

Starter de Gatsby con `magma-elements` y `magma-theme`.

- [ Cómo empezar ](#cómo-empezar)
- [ Iconos ](#iconos)

## Cómo empezar

1. **Crear un proyecto de Gatsby con el starter**

Usa el comando de terminal `gatsby` para crear un proyecto y especificar el starter. Es necesario tener acceso a este repositorio en bitbucket.

> Debes cambiar `<PROJECT-NAME>` por el nombre que elijas para tu proyecto y `<BITBUCKET-USERNAME>` por el nombre de usuario de bitbucket.

```shell
gatsby new <PROJECT-NAME> https://<BITBUCKET-USERNAME>@bitbucket.org/secuoyas/gatsby-starter-magma.git
```

2. **Arranca el servidor.**

Navega al directorio que Gatsby acaba de crear

```shell
cd <PROJECT-NAME>
```

Arranca el servidor

**yarn**

```shell
yarn start
```

**npm**

```shell
npm start
```

3.  **Abre la página en un navegador**

Por defecto el servidor arranca en `http://localhost:8000`

_Note: Verás también un segundo enlace: _`http://localhost:8000/___graphql`_. Este link es para experimentar y testear con GraphQL. Aprende más cómo usar esta herramienta en [Gatsby tutorial](https://www.gatsbyjs.org/tutorial/part-five/#introducing-graphiql)._

4. **Empieza a editar**

Abre el archivo `<PROJECT-NAME>/src/pages/index.js` en tu editor de código. Guarda los cambios y el navegador se refrescará en "tiempo real".

## Iconos

Esta es una pequeña guía de cómo incluir una biblioteca de iconos svg propia en React.

### Cómo crear la biblioteca de iconos
Para crear la biblioteca necesitamos algunos pasos manuales (por ahora)

Primero veamos los archivos y carpetas que intervienen.

#### Build icons

Este script escrito en `node` se encuentra en `scrtips/IconSystem/buildIcons.js`. Es el script que lanzaremos para construir la biblioteca de iconos.

Lo lanzaremos con:

```bash
node scripts/IconSystem/buildIcons.js
```

#### Especificación de iconos

En el archivo `scrtips/IconSystem/iconSpec.json` escribiremos una entrada por cada icono que necesitemos en la biblioteca, definiendo ciertos parámetros.

```json
{
  "path": "./svg",
  "extension": ".svg",
  "icons": [
    { "fileName": "icn-magma-logo", "name": "logo", "import": "Logo", "type": "fill" },
    { "fileName": "icn-gatsby-logo", "name": "logoGatsby", "import": "GatsbyLogo", "type": "fill" }
  ]
}
```

**fileName** nombre (sin la ruta) del archivo svg
**name** nombre que después utilizaremos para llamar al icono utilizando el componente `<IcnSystem />`
**import** nombre que después utilizaremos para llamar al icono utilizando el componente directamente, por ejemplo `<FacebookLogo />`. Deberá estar escrito en PascalCase.
**type** por ahora sin uso. Se recomienda poner `fill`

#### Archivos SVG's

En la carpeta `src/components/IconSystem/svg` guardaremos los archivos svgs. El nombre del archivo puede ser cualquiera.

> El archivo svg debe estar "rasterizado" sin strokes y no debe contener los atributos `width` y `height`

#### Componente `<IcnSystem>`

El componente se creará automáticamente. Es el que después usaremos (como veremos más abajo) para llamar a los iconos de la biblioteca.

#### Icons.js

Este componente también se genera automáticamente y puede ser importado para llamar a los iconos de manera explícita con nombre propio.

#### Resumen

Los pasos serían.

1. Guardar el icono en la carpeta
2. Escribir la entrada en el archivo `iconSpec.json`
3. Lanzar el script con `node scripts/IconSystem/buildIcons.js

### Cómo se usa

**Utilizando el componente <IcnsSytem />**

```js
import { IcnSystem } from "../components/IcnSystem"

const Foo = () => (
  <div>
    <IcnSystem name="facebookLogo" height="24px" fill="text01" />
  </div>
)
```

**Utilizando directamente el componente del icono**

```js
import { FacebookLogo } from "../components/IcnSystem/icons"

const Foo = () => (
  <div>
    <FacebookLogo height="16px" fill="text01" />
  </div>
)
```
