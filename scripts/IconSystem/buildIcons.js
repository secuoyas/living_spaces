const fs = require("fs")
const spec = JSON.parse(fs.readFileSync(__dirname + "/iconsSpec.json"))
const fileHeader = `
import React from "react"
import styled, { css } from "styled-components"
import { space, color, layout } from "styled-system"
`

const fileImport = icons => {
  return icons
    .map(icon => {
      return `import ${icon.import} from "./svg/${icon.fileName}.svg"`
    })
    .join("\n")
}

const baseDefinition = `
const Base = css\`
  $\{space\}
  $\{color\}
  $\{layout\}
  *\ {
    fill: $\{p => p.fill\};
    fill: $\{p => p.theme.color[p.fill]\};
  }
\`
`

const styledIcon = icons => {
  return icons
    .map(icon => {
      return `const Styled${icon.import} = styled(${icon.import})\`$\{Base\}\``
    })
    .join("\n")
}

const baseIcon = icons => {
  return icons
    .map(icon => {
      return `
const Base${icon.import} = (\{ name, mode, fill, ...props \}) => (
  <Styled${icon.import} name=\{name\} mode=\{mode\} fill=\{fill\} \{...props\} />
)`
    })
    .join("\n")
}

const exportIcon = icons => {
  return icons
    .map(icon => {
      return `export \{ Base${icon.import} as ${icon.import} \}`
    })
    .join("\n")
}

const icons = spec.icons

// icons.js
const iconsFile = `
${fileHeader}
${fileImport(icons)}
${baseDefinition}
${styledIcon(icons)}

${baseIcon(icons)}

${exportIcon(icons)}
 `

const IcnSystemImportReact = `import React from "react"`
// IcnSystem.js

const IcnSystemImportIcons = icons => {
  const importNames = icons
    .map(icon => {
      return icon.import
    })
    .join(", ")
  return `import \{ ${importNames} \} from "./icons"`
}

const IcnSystemIcons = icons => {
  const iconNames = icons.map(icon => {
    return `${icon.name}: ${icon.import}`
  })
  return `const Icons = \{${iconNames}\}`
}

const IcnComponent = `const IcnSystem = (\{ name, ...props \}) => \{
   return Icons[name](props)
}
`

const icnSystemFile = `
${IcnSystemImportIcons(icons)}
${IcnSystemIcons(icons)}
${IcnComponent}
export \{ IcnSystem \}
`

fs.writeFileSync(__dirname + "/../../src/components/IconSystem/icons.js", iconsFile)
fs.writeFileSync(__dirname + "/../../src/components/IconSystem/IcnSystem.js", icnSystemFile)
