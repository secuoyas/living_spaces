import React, { useRef, useState, useEffect } from "react"
import { Box } from "@sqymagma/elements"
import styled from "styled-components"
import { motion, useMotionValue } from "framer-motion"
import Knob from "./knob"

const ImgFondo = styled.img`
  position: absolute;
  top: 0;
  right: 0;
  object-fit: cover;
  width: 100%;
  height: 100%;
  max-width: 100%;
`
const ImgTop = styled(Box)`
  position: absolute;
  top: 0;
  left: 0;
  background-image: url(${p => p.src});
  background-position: left;
  height: 100%;
  background-repeat: no-repeat;
  background-size: cover;
  max-width: 100%;
`

const CSSHandler = styled(motion.div)`
  width: 100%;
  max-width: 100%;
  height: 100%;
  overflow: hidden;
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  place-content: center;
  overflow: hidden;
`

const Cssitemframe = styled(motion.div)`
  width: 8px;
  height: 100%;
  background: rgba(255, 255, 255, 0.2);
  border-radius: inherit;
  display: flex;
  justify-content: center;
  flex-direction: column;
`

const AntesVsDespues = ({ srcAntes, altAntes, srcDespues, altDespues, tagidhandler }) => {
  const cref = useRef(null)
  const contref = useRef(null)
  const [w, setW] = useState(0)
  const [ancho, setAncho] = useState(800)
  useEffect(() => {
    setAncho(contref.current.parentElement.clientWidth)
  }, [])
  const x = useMotionValue(0)
  x.onChange(e => {
    setW(e)
  })

  return (
    <Box width="100%" maxWidth="100%" ref={contref}>
      <ImgFondo src={srcDespues} alt={altAntes} />
      <ImgTop src={srcAntes} width={ancho / 2 + w + "px"} />
      <CSSHandler ref={cref} id={tagidhandler}>
        <Cssitemframe drag="x" dragConstraints={cref} style={{ x }}>
          <Knob />
        </Cssitemframe>
      </CSSHandler>
    </Box>
  )
}

export default AntesVsDespues
