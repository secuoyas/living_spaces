import React from "react"
import styled from "styled-components"

const Breakpoint = styled.div`
  width: 50px;
  height: 50px;
  border: 2px solid #000;
  background-color: #fff;
  position: fixed;
  bottom: 20px;
  right: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 999999;
  box-shadow: 0px 13px 37px -4px rgba(0, 0, 0, 0.22);
  &::after {
    content: "XS";
    ${p => p.theme.mq.s} {
      content: "S";
    }
    ${p => p.theme.mq.m} {
      content: "M";
    }
    ${p => p.theme.mq.l} {
      content: "L";
    }
    ${p => p.theme.mq.xl} {
      content: "XL";
    }
  }
`

const BP = () => {
  return <Breakpoint />
}

export default BP
