import React from "react"
import { Box, Text } from "@sqymagma/elements"

const cSubtitle = ({ children }) => (
  <Box pt="64px" pb="24px">
    <Text textStyle="h5">{children}</Text>
  </Box>
)

export default cSubtitle
