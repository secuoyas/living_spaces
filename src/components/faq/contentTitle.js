import React from "react"
import { Box, Text } from "@sqymagma/elements"

const cTitle = ({ children }) => (
  <Box pt="48px" pb="48px">
    <Text textStyle="h4" weight="500">
      {children}
    </Text>
  </Box>
)

export default cTitle
