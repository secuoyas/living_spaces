import React from "react"
import { Box } from "@sqymagma/elements"
import styled from "styled-components"

const CSSKnob = styled(Box)`
  width: 8px;
  border: 1px solid #ccc;
  background: white;
  border-radius: 30px;
  display: flex;
  justify-content: center;
  padding: 24px 2px 24px 2px;
  flex-direction: column;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.08), 0px 4px 4px rgba(0, 0, 0, 0.08);
`
const CSSLine = styled(Box)`
  width: 100%;
  height: 1px;
  margin: 1px auto 1px auto;
  background: #ccc;
`

const Knob = (ref, ...props) => {
  return (
    <Box>
      <CSSKnob>
        <CSSLine />
        <CSSLine />
        <CSSLine />
      </CSSKnob>
      <Box />
    </Box>
  )
}

export default Knob
