import React from "react"

const Linkexternal = ({ href, rel, children, ...props }) => (
  <a href={href} rel={rel} target="_blank" {...props}>
    {children}
  </a>
)

export default Linkexternal
