import { graphql, useStaticQuery } from "gatsby"

const Imgs = useStaticQuery(
  graphql`
    query {
      g1: file(relativePath: { eq: "juan-de-iziar/g1.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1920, quality: 100) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
      g2: file(relativePath: { eq: "juan-de-iziar/g2.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1920, quality: 100) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
      g3: file(relativePath: { eq: "juan-de-iziar/g3.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1920, quality: 100) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
      g4: file(relativePath: { eq: "juan-de-iziar/g4.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1920, quality: 100) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }

      header: file(relativePath: { eq: "juan-de-iziar/hero.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 490, quality: 100) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
      a1: file(relativePath: { eq: "juan-de-iziar/g4.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 360, quality: 100) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
      a2: file(relativePath: { eq: "juan-de-iziar/g3.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 360, quality: 100) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
      b1: file(relativePath: { eq: "juan-de-iziar/g1.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 840, quality: 100) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
      b2: file(relativePath: { eq: "juan-de-iziar/g2.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 840, quality: 100) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  `
)

export default Imgs
