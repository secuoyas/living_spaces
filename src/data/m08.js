const m08 = {
  title: "Nos encargamos de todo",
  text:
    "Una vez reformadas y decoradas las viviendas ofrecemos a nuestros clientes inversores la posibilidad de gestionar íntegramente todos los aspectos relacionados con los alquileres y la gestión de los inquilinos y el cobro de la rentas.",
  cards: [
    {
      title: "Búsqueda de Inquilino.",
      icon: "icnlimpia",
      desc: "Selección y negociación del contrato de alquiler.",
    },
    {
      title: "Gestión del inquilino y la CCPP.",
      icon: "icnestado",
      desc: "Revisión del cobro de la renta e incidencias con los inquilinos y CCPP.",
    },
    {
      title: "Seguro del cobro de rentas.",
      icon: "icnpromo",
      desc: "Aseguramos las rentas de los alquileres.",
    },
    {
      title: "Financiación de la vivienda.",
      icon: "icninquilino",
      desc: "Buscamos la mejor hipoteca del mercado para financiar la inversión.",
    },
  ],
}

export default m08
