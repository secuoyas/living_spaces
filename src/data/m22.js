const m22 = {
  title: "Sobre Living Alternatives",
  p01:
    "Somos un estudio especializado en la gestión de cambios de uso en Madrid.  Adquirimos locales comerciales para transformarlos posteriormente en viviendas reformadas y decoradas que generan altos porcentajes de rentabilidad.  Con la finalidad de ayudar a las personas que buscan oportunidades de inversión en este tipo de inmubles, hemos (r)evolucionado la manera tradicional de hacer las cosas.",
  p02:
    "Asumiendo la responsabilidad íntegra de la gestión para que los inversores puedan ahorrar meses de trámites, costes y tiempos. Queremos simplificar el proceso de gestión de la inversión, para que nuestros clientes puedan centrarse en lo verdaderamente importante: la rentabilidad del inmueble. Aprovechamos nuestra experiencia en el ámbito tecnológico, financiero y del diseño para aportar el mayor valor posible en cada una de las fases del proceso.",
}

export default m22
