import React from "react"
import { MaxWidth } from "@sqymagma/elements"
import styled from "styled-components"
import Block from "../components/block"

const EmptyBlock = styled.div`
  height: 300px;
  border: 2px solid;
  margin: 96px auto;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  ${p => p.theme.textStyle.p}
  color: ${p => p.theme.color.brand01};
`

const Empty = ({ children }) => {
  return (
    <MaxWidth>
      <Block>
        <EmptyBlock> {children} </EmptyBlock>
      </Block>
    </MaxWidth>
  )
}

export default Empty
