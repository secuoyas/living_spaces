import React from "react"
import styled from "styled-components"
import { IcnSystem } from "../components/IconSystem/IcnSystem"
import Img from "gatsby-image"
import Btn from "../components/button"
import { Box, Text, Columns, Flex } from "@sqymagma/elements"
import BgImg from "gatsby-background-image-es5"

const MainImg = styled(Img)`
  z-index: 0;
  position: relative;
  height: 60vh;
  @media (min-width: 1024px) {
    height: 84vh;
  }
`

const CssBotShortText = styled(Box)`
  display: none;
  ${p => p.theme.mq.s} {
    display: block;
    padding-right: 4px;
  }
`

const Borders = styled(Flex)`
  border-top: 1px solid;
  border-bottom: 1px solid;
  border-color: ${p => p.theme.color.bg02};
`

const ContCardPrice = styled(Flex)`
  border: 8px solid;
  margin: 0 auto;
  border-color: ${p => p.theme.color.bg02};
  position: relative;
  @media (min-width: 1024px) {
    width: 360px;
  }
  @media (min-width: 1280px) {
    width: 472px;
  }
  @media (min-width: 1920px) {
    width: 592px;
  }
`

const MapLocation = MapPhoto => {
  return (
    <Box className="map" height={["112px", null, "136px"]} width="100%" my="spc" mb="spc3"></Box>
  )
}
const Item = ({ name, price }) => {
  return (
    <Flex flexDirection="row" justifyContent="space-between" py="spc_2" width="100%">
      <Text textStyle="p_t" textAlign="left">
        {name}
      </Text>
      <Text textStyle="p_t">{price}</Text>
    </Flex>
  )
}
const BoldItem = ({ name, price }) => {
  return (
    <Flex flexDirection="row" justifyContent="space-between" py="spc_2" width="100%">
      <Text textStyle="p_t" fontWeight="500 !important" textAlign="left">
        {name}
      </Text>
      <Text textStyle="p_t" fontWeight="500 !important">
        {price}
      </Text>
    </Flex>
  )
}
const CardPrice = ({ d, eltitulo, total, infotext }) => {
  const ptotal = typeof d.nviv !== "undefined" ? " " + d.nviv + ":" : ":"

  return (
    <ContCardPrice bg="text02">
      <Flex
        flexDirection="column"
        textAlign="center"
        m={["spc2", "spc3", "spc3", "spc3", "spc4"]}
        width={["100%", "100%", "344px", "456px", "576px"]}
      >
        <Text textStyle="h6">{eltitulo + " " + (typeof d.nviv !== "undefined" ? d.nviv : "")}</Text>
        <Text textStyle="h2" color="brand03">
          {total}
        </Text>
        <Box bg="bg02" height="2px" my="spc2"></Box>
        <Columns cols={[1, 2, 1]} hs={["0px", "32px", "0px"]} width="100%">
          <BoldItem name={"Presupuesto de inversión "} price="" />
          <Item name="Precio de compra" price={d.precioCompra} />
          <Item name="Gastos asociados a la compra" price={d.gastosCompra} />
          <Item name="Reforma y decoración" price={d.gastoReforma} />
          <Item name="Otros gastos (Arquitectura, Licencias, Impuestos)" price={d.gastosOtros} />
          <BoldItem name={"Total inversión" + ptotal} price={d.inversionTotal} />
          {typeof d.totalud !== "undefined" && (
            <BoldItem name="Inversión por vivienda resultante:" price={d.totalud} />
          )}
          <Box p="12px" w="100%" /> <Item name="Ingresos brutos anuales" price={d.ingresosBrutos} />
          <BoldItem name="Rentabilidad bruta del alquiler" price={d.rentabilidadBruta} />
          <Box p="12px" w="100%" />
          <Item name="Financiación bancaria obtenida" price={d.financiacion} />
          <BoldItem name="Rentabilidad bruta s/ fondos propios" price={d.rentabilidadPropia} />
        </Columns>
        <Text pt="spc3" textAlign="left" textStyle="p1_t">
          {infotext}
        </Text>
      </Flex>
    </ContCardPrice>
  )
}

const IconsBox = ({ m2, aseos, habitaciones, ac }) => {
  return (
    <Columns cols={[2, 4]} textAlign="left">
      <Flex alignItems="center">
        <IcnSystem
          name="icnescuadra"
          height={["24px", "36px"]}
          fill="brand03"
          margin=" 0 4px 0 16px"
        />
        <Text textStyle="p1" mx={["spc_2", "spc"]}>
          M2
        </Text>{" "}
        <Text textStyle="p1" color="brand03">
          {m2}
        </Text>
      </Flex>
      <Flex alignItems="center">
        <IcnSystem
          name="icndormitorio"
          height={["24px", "36px"]}
          fill="brand03"
          margin=" 0 4px 0 16px"
        />
        <Text textStyle="p1" mx={["spc_2", "spc"]}>
          Hab.
        </Text>{" "}
        <Text textStyle="p1" color="brand03">
          {habitaciones}
        </Text>
      </Flex>
      <Flex alignItems="center">
        <IcnSystem name="icnaseo" height={["24px", "36px"]} fill="brand03" margin=" 0 4px 0 16px" />
        <Text textStyle="p1" mx={["spc_2", "spc"]}>
          Baños
        </Text>{" "}
        <Text textStyle="p1" color="brand03">
          {aseos}
        </Text>
      </Flex>
      <Flex alignItems="center">
        <IcnSystem name="icnac" height={["24px", "36px"]} fill="brand03" margin=" 0 4px 0 16px" />
        <Text textStyle="p1" mx={["spc_2", "spc"]}>
          ac
        </Text>{" "}
        <Text textStyle="p1" color="brand03">
          {ac ? <p>Si</p> : <p>No</p>}
        </Text>
      </Flex>
    </Columns>
  )
}

const Main = ({
  title,
  desc,
  m2,
  aseos,
  habitaciones,
  ac,
  vr360,
  openGallery,
  Mapa,
  tagid07360,
  tagid07fotos,
}) => {
  return (
    <div>
      <Flex justifyContent="flex-end" px={[0, null, "16px"]} className="botones">
        <Btn
          as="a"
          type="link"
          size={["small", "big"]}
          margin="0 16px 0 0"
          id={tagid07360}
          href={vr360}
          title="Visita 360 de la vivienda"
          target="_blank"
          rel="noopener noreferrer"
        >
          <CssBotShortText>Visita Virtual </CssBotShortText> 360º{" "}
          <IcnSystem
            name="openoutside"
            height="16px"
            fill="inverse01"
            css="flex-shrink: 0; margin-left:8px"
          />
        </Btn>
        <Btn
          type="secondary"
          size={["small", "big"]}
          margin="0"
          onClick={() => openGallery()}
          id={tagid07fotos}
          title="Galería de fotos"
        >
          Fotos{" "}
          <IcnSystem
            name="photos"
            height="16px"
            fill="brand01"
            css="flex-shrink: 0; margin-left:8px"
          />
        </Btn>
      </Flex>
      <Box
        className="content"
        bg="text02"
        mt="spc"
        ml={["0px", "0px", "spc3", "spc3", "0px"]}
        px={["spc", "spc2", "spc3", "spc3", "104px"]}
        py={["spc1", "spc1", "spc2", "spc3", "spc6"]}
      >
        <Box id="title" pb={["spc_1", "spc_1", "spc_2", "spc", "spc2"]}>
          <Text textStyle="h1">{title}</Text>
        </Box>
        <BgImg fluid={Mapa.childImageSharp.fluid}>
          <MapLocation />
        </BgImg>
        <Borders mb="spc3" height={"128px"} alignItems="center" justifyContent="center">
          <IconsBox m2={m2} aseos={aseos} habitaciones={habitaciones} ac={ac} />
        </Borders>

        <Columns cols={[1, 2]} hs={["0px", "32px"]} my="spc">
          <Text textStyle="p">{desc[0]}</Text>
          <Text textStyle="p">{desc[1]}</Text>
        </Columns>
      </Box>
    </div>
  )
}

const Hero = ({ data, HeroPhoto, openGallery, vr360, Mapa }) => {
  return (
    <>
      <MainImg fluid={HeroPhoto.childImageSharp.fluid} alt={data.altimage} />
      <Flex
        zIndex={4}
        mt={["-88px", "-104px", "-104px", "-104px", "-200px"]}
        width={["calc(100% - 32px)", "calc(100% - 80px)", "1024px", "1280px", "1648px"]}
        flexDirection={["column", "column", "row"]}
        alignItems="start"
        mx="auto"
        pb={["spc", "spc2", "spc2", "spc3", "spc4"]}
        position="relative"
      >
        <Main
          zIndex={4}
          title={data.titulo}
          width={["304px", "586px", "520px", "630px", "848px"]}
          mx="auto"
          position="relative"
          flex="1 1 auto"
          desc={data.desc}
          address={data.direccion}
          m2={data.datos.m2}
          aseos={data.datos.aseos}
          habitaciones={data.datos.habitaciones}
          ac={data.datos.ac ? "si" : "no"}
          openGallery={() => openGallery()}
          vr360={vr360}
          Mapa={Mapa}
        />
        <CardPrice
          zIndex={4}
          flex="0 0 0"
          d={data.datos}
          eltitulo={data.datos.eltitulo}
          total={data.datos.total}
          infotext={data.datos.infotext}
        />
      </Flex>
    </>
  )
}

export default Hero
