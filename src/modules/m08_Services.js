import React from "react"
import { Flex, Box, Text, Columns } from "@sqymagma/elements"
import Block from "../components/block"
import styled from "styled-components"
import Tick from "../components/tick"
import { IcnSystem } from "../components/IconSystem/IcnSystem"

import data from "../data/m08"

const CssPostIt = styled(Flex)`
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.08), 0px 4px 4px rgba(0, 0, 0, 0.08);
`

const BorderBox = styled(Flex)`
  border: 8px solid;
  border-color: ${p => p.theme.color.bg01};
`

const Card = ({ d }) => {
  return (
    <Flex
      textAlign={["left", "left", "left", "center"]}
      flexDirection={["row", "row", "row", "column"]}
      my={["spc", "spc1", "spc1", 0, 0]}
      px={["spc1", null, "0", "spc1"]}
      py={["0", null, "0", "spc1"]}
    >
      <IcnSystem name={d.icon} height="64px" fill="brand03" css="flex-shrink: 0;" />
      <Box ml={["spc", "spc", "spc", 0]} mt={[0, 0, 0, "spc2"]}>
        <Box>
          <Text textStyle="h6">{d.title}</Text>
        </Box>
        <Box pt={["spc_2", "spc_2", "spc_2", "spc", "spc1"]} />
        <Box>
          <Text textStyle="p1">{d.desc}</Text>
        </Box>
      </Box>
    </Flex>
  )
}

const ModServices = () => {
  const Services = data.cards.map(d => <Card d={d} />)
  return (
    <Box py={["spc3", "spc4", "spc6", "spc8", "spc8"]} position="relative">
      <Block>
        <BorderBox p={["spc", "spc1", "spc3"]} flexDirection={["column", null, "row"]}>
          <CssPostIt
            bg="brand02"
            mt={["-32px", "-40px", "0px"]}
            mr={["0", null, "-64px"]}
            order={["1", null, "2"]}
            flexShrink={["0"]}
            width={["100%", null, "364px", "400px", "500px"]}
            mb={["spc1", null, "0"]}
          >
            <Box p={["spc2", "spc3", "spc3", "spc3", "spc4"]}>
              <Text textStyle="h4">{data.title}</Text>
              <Tick />
              <Text textStyle="p">{data.text}</Text>
            </Box>
          </CssPostIt>
          <Flex flex={[null, null, "1"]} order={["2", null, "1"]} pr={[null, null, "spc1"]}>
            <Columns
              pt={[null, null, null, "16px", "56px"]}
              mb={["spc1", null, "0"]}
              cols={[1, 2, 2, 4, 4]}
            >
              {Services}
            </Columns>
          </Flex>
        </BorderBox>
      </Block>
    </Box>
  )
}

export default ModServices
