import React from "react"
import { Box, Text, Flex } from "@sqymagma/elements"
import BgImg from "gatsby-background-image-es5"

import Block from "../components/block"
import Tick from "../components/tick"
import styled from "styled-components"
import Btn from "../components/button"

const Grid = styled(Box)`
  display: grid;
  grid-template-columns: 1fr 3fr 4fr 3fr 1fr;
  grid-template-rows: 4fr 3fr 3fr 4fr;
`

const A1 = styled(BgImg)`
  grid-column: 2 / 4;
  grid-row: 1 / 3;
`
const A2 = styled(BgImg)`
  grid-column: 4 / 5;
  grid-row: 2 / 3;
`
const B1 = styled(BgImg)`
  grid-column: 2 / 3;
  grid-row: 3 / 4;
`
const B2 = styled(BgImg)`
  grid-column: 3 / 5;
  grid-row: 3 / 5;
`

const Fotos = ({ d, Imgs, openGallery, tagid10360, tagid10Fotos, alt01, alt02, alt03, alt04 }) => {
  const res = d.resultado.map(descr => {
    return (
      <Box mb={("spc_2", "spc", "spc", "spc1", "spc2")}>
        <Text textStyle="p_t">{descr}</Text>
      </Box>
    )
  })
  return (
    <Block>
      <Box
        px={["0", "spc4", "spc6", "spc12", "spc16"]}
        py={["spc4", "spc4", "spc4", "spc6", "spc6"]}
        textAlign="center"
      >
        <Box>
          <Text textStyle="h3">Resultado</Text>
        </Box>
        <Tick align="center" />
        {res}
      </Box>

      <Grid
        gridGap={["8px", "24px", "32px", "32px", "32px"]}
        height={["200px", "300px", "600px", "800px", "1000px"]}
      >
        <A1 fluid={Imgs.a1.childImageSharp.fluid} aria-label={alt01}>
          <Flex justifyContent="center" alignItems="center" width="100%" height="100%">
            <Btn
              as="a"
              type="link"
              href={d.vr360}
              alt="visita 360 de la vivienda"
              target="_blank"
              rel="noopener noreferrer"
              id={tagid10360}
              title="Visita 360 de la vivienda"
            >
              <Text display={["none", "block"]} pr="4px">
                Visita virtual
              </Text>{" "}
              360º
            </Btn>
          </Flex>
        </A1>
        <A2 fluid={Imgs.a2.childImageSharp.fluid} aria-label={alt02} />
        <B1 fluid={Imgs.b1.childImageSharp.fluid} aria-label={alt03}></B1>
        <B2 fluid={Imgs.b2.childImageSharp.fluid} aria-label={alt04}>
          <Flex justifyContent="center" alignItems="center" width="100%" height="100%">
            <Btn
              type="secondary"
              onClick={() => openGallery()}
              id={tagid10Fotos}
              title="Galería de fotos"
            >
              Fotos
            </Btn>
          </Flex>
        </B2>
      </Grid>
    </Block>
  )
}

export default Fotos
