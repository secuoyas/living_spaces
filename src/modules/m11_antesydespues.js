import React from "react"
import { Flex, Box, Text } from "@sqymagma/elements"
import Block from "../components/block"
import styled from "styled-components"
import Tick from "../components/tick"
import AntesVsDespues from "../components/antesVsDespues"
import AspecRatio from "../components/AspectRatioBox"

const CssWhiteBox = styled(Flex)`
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.08), 0px 4px 4px rgba(0, 0, 0, 0.08);
  ${p => p.theme.mq.s} {
    transform: translateY(-50%);
  }
`

const WhiteBox = ({ title, subtitle }) => {
  return (
    <CssWhiteBox
      position={["relative", "absolute"]}
      left={["inherit", "0"]}
      top={["inherit", "50%"]}
      bg="primaryBackground"
      mt={["spc2", "0", "0px"]}
      ml={["0", "-16px", "0"]}
      mr={["0", null, "-64px"]}
      width={["100%", "280px", "344px", "376px", "632px"]}
      mb={["-16px", "0"]}
      zIndex="2"
      p={["spc1", "spc1", "spc2", "spc2", "spc3"]}
      flexDirection="column"
    >
      <Text textStyle="h4">{title}</Text>
      <Tick py={["spc1", "spc1"]} />
      <Text textStyle="p">{subtitle}</Text>
    </CssWhiteBox>
  )
}

const M11FotosBefore = ({
  srcAntes,
  altAntes,
  srcDespues,
  altDespues,
  title,
  subtitle,
  tagidhandler,
}) => {
  return (
    <Box py="10vw" position="relative">
      <Block>
        <Box position="relative">
          <WhiteBox title={title} subtitle={subtitle} />
          <AspecRatio
            type="16to9"
            bg="black"
            position="relative"
            overflow="hidden"
            width={["106.9%", "100%", "768px", "976px", "976px"]}
            ml={["-3.5%", "0%", "auto"]}
            maxWidth={["106.9%", "100%"]}
          >
            <AntesVsDespues
              srcAntes={srcAntes}
              srcDespues={srcDespues}
              altAntes={altAntes}
              altDespues={altDespues}
              tagidhandler={tagidhandler}
            />
          </AspecRatio>
        </Box>
      </Block>
    </Box>
  )
}

export default M11FotosBefore
