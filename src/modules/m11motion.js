import React from "react"
import { useRef } from "react"
import { motion, useMotionValue, useTransform } from "framer-motion"
import styled from "styled-components"

const Cssbigboxy = styled(motion.div)`
  width: 100vw;
  height: 100vh;
  overflow: hidden;
  padding: 0;
  margin: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  perspective: 500px;
  background: linear-gradient(180deg, #70f, #40f);
`

const Csscontainer = styled(motion.div)`
  width: 300px;
  height: 150px;
  display: flex;
  place-content: center;
  overflow: hidden;
  background: rgba(255, 255, 255, 0.2);
  border-radius: 30px;
`

const Cssitem = styled(motion.div)`
  width: 150px;
  height: 150px;
  background: white;
  border-radius: inherit;
`

const M11Motion = () => {
  const constraintsRef = useRef(null)
  const x = useMotionValue(0)
  const rotateY = useTransform(x, [-200, 0, 200], [-45, 0, 45], {
    clamp: false,
  })

  return (
    <Cssbigboxy>
      <Csscontainer
        className="container"
        ref={constraintsRef}
        style={{
          rotateY,
        }}
      >
        <Cssitem
          className="item"
          drag="x"
          dragConstraints={constraintsRef}
          style={{
            x,
          }}
        />
      </Csscontainer>
    </Cssbigboxy>
  )
}

export default M11Motion
