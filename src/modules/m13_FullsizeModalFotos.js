import React from "react"
import { Text, Box } from "@sqymagma/elements"
import styled from "styled-components"
import NukaCarousel from "nuka-carousel"
import Btn from "../components/button"
import BtnNav from "../components/buttonNav"
import { IcnSystem } from "../components/IconSystem/IcnSystem"

const GalleryContainer = styled(Box)`
  transition: all 0.5s ease;
  & .slider-control-topright {
    top: calc(100vh - 128px) !important;
    @media (min-width: 768px) {
      top: calc(100vh - 256px) !important;
    }
  }
`

const StyledImage = styled.img`
  object-fit: cover;
  object-position: center;
  width: 100%;
  height: 100%;
`

const Gallery = ({ galleryOpen, onClose, imgGallery, address }) => {
  return (
    <GalleryContainer
      display="block"
      bg="bg01"
      width="100vw"
      height={galleryOpen ? "100vh" : "0vh"}
      opacity={galleryOpen ? "1" : "0"}
      position="fixed"
      top="0"
      bottom="0"
      right="0"
      left="0"
      zIndex="9999999"
      overflow="hidden"
    >
      <BtnNav
        open={false}
        onClick={onClose}
        position="absolute"
        right={["24px", "40px"]}
        top={["24px", "40px"]}
        fill="bg03"
      />
      <NukaCarousel
        renderCenterLeftControls={null}
        renderCenterRightControls={null}
        defaultControlsConfig={{
          pagingDotsStyle: {
            fill: "white",
          },
        }}
        renderTopRightControls={({ nextSlide }) => (
          <Btn
            opacity="0.8"
            onClick={nextSlide}
            width={["64px", "128px"]}
            height={["64px", "128px"]}
            padding="0"
          >
            <IcnSystem name="aright" height={["24px", "32px"]} fill="text02" />
          </Btn>
        )}
        renderBottomRightControls={({ previousSlide }) => (
          <Btn
            opacity="0.8"
            onClick={previousSlide}
            width={["64px", "128px"]}
            height={["64px", "128px"]}
            padding="0"
          >
            <IcnSystem name="aleft" height={["24px", "32px"]} fill="text02" />
          </Btn>
        )}
      >
        {imgGallery.map((item, idx) => {
          return (
            <Box key={idx} width="100vw" height="100vh">
              <Box position="absolute" top="0" bg="bg03" p="8px 48px">
                <Text textStyle="p1" color="text01" width={["250px", "420px", "530px"]}>
                  <Text color="brand01" fontWeight="500">
                    Foto {idx + 1} :
                  </Text>
                  {address}
                </Text>
              </Box>
              <StyledImage src={item} alt={address + "- foto " + idx} />
            </Box>
          )
        })}
      </NukaCarousel>
    </GalleryContainer>
  )
}

export default Gallery
