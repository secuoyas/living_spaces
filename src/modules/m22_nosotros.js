import React from "react"
import { Box, Columns, MaxWidth, Text } from "@sqymagma/elements"
import Tick from "../components/tick"
import Block from "../components/block"

import data from "../data/m22"

const Nosotros = () => {
  return (
    <MaxWidth textAlign="center" py={["spc4", "spc4", "spc6", "spc8", "spc16"]}>
      <Block>
        <Text textStyle="h3">{data.title}</Text>
        <Tick align="center" />
        <Columns cols={[1, 2]} textAlign="left">
          <Box px={["spc", null, null, null, "spc1"]} py="spc">
            <Text textStyle="p">{data.p01}</Text>
          </Box>
          <Box px={["spc", null, null, null, "spc1"]} py="spc">
            <Text textStyle="p">{data.p02}</Text>
          </Box>
        </Columns>
      </Block>
    </MaxWidth>
  )
}

export default Nosotros
