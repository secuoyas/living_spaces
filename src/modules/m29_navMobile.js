import React, { useState } from "react"
import { Flex, Box, Text } from "@sqymagma/elements"
import styled from "styled-components"
import Tick from "../components/tick"
import { Link } from "gatsby"
import ButtonNav from "../components/buttonNav"

const F = styled(Box)`
  position: fixed;
  z-index: 200;
  display: ${p => (p.open ? "block" : "none")};
`

const A = styled(Link)`
  color: ${p => p.theme.color.brand01};
  ${p => p.theme.textStyle.subtitle};
  margin-bottom: 16px;
  ${p => p.theme.mq.xl} {
    margin-bottom: 32px;
  }
`

const NavMobile = () => {
  const [open, setOpen] = useState(false)
  return (
    <>
      <ButtonNav onClick={() => setOpen(!open)} open={open} />
      <F open={open} py="spc4" textAlign="center" bg="bg01" width="100%" height="100vh" top="0px">
        <Flex width="100%" height="100vh" justifyContent="center" alignItems="center">
          <div>
            <Text textStyle="h3" color="text01">
              Menú
            </Text>
            <Tick align="center" />
            <Flex flexDirection="column">
              <A to="/invertir/">Invertir en locales</A>
              <A to="/como/ ">Como lo hacemos</A>
              <A to="/casos/">Casos</A>
              <A to="/faq/">FAQ</A>
              <A to="/somos/">Quienes somos</A>
            </Flex>
          </div>
        </Flex>
      </F>
    </>
  )
}

export default NavMobile
