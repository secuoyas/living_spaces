import React from "react"
import { Text } from "@sqymagma/elements"
import styled from "styled-components"

const Bg = styled.div`
  background-color: ${p => p.theme.color.bg01};
  width: 100%;
  padding: 4rem;
  ${p => p.theme.textStyle.display01}
  ${p => p.theme.mq.xl} {
    background-color: red;
  }
`

const SelText = styled(Text)`
  background-color: ${p => p.theme.color.brand02};
  color: ${p => p.theme.color.text01};
  display: inline-block;
`

const Typo = ({ children }) => {
  return (
    <Bg>
      <SelText textStyle="display01">Estilo de texto Display01</SelText>
      <br />

      <SelText textStyle="h1">Estilo de texto H1</SelText>
      <br />

      <SelText textStyle="h2">Estilo de texto H2</SelText>
      <br />

      <SelText textStyle="h3">Estilo de texto H3</SelText>
      <br />

      <SelText textStyle="h4">Estilo de texto H4</SelText>
      <br />

      <SelText textStyle="h5">Estilo de texto H5</SelText>
      <br />

      <SelText textStyle="h6">Estilo de texto H6</SelText>
      <br />

      <SelText textStyle="subtitle">Estilo de texto subtitle</SelText>
      <br />

      <SelText textStyle="p">Estilo de texto p</SelText>
      <br />

      <SelText textStyle="p_t">Estilo de texto p-thick</SelText>
      <br />

      <SelText textStyle="p1">Estilo de texto p1</SelText>
      <br />

      <SelText textStyle="p1_t">Estilo de texto p1-thick</SelText>
      <br />

      <SelText textStyle="p2">Estilo de texto p2</SelText>
      <br />

      <SelText textStyle="p2_t">Estilo de texto p2-thick</SelText>
      <br />
    </Bg>
  )
}

export default Typo
