import React from "react"
import NotFound from "../modules/m28_page404"

const NotFoundPage = () => <NotFound tagid404bot="TM75-404-MóduloRedirigir-BotonHome" />

export default NotFoundPage
