import React from "react"
import { storiesOf } from "@storybook/react"
import Button from "../components/Button"
import TestWithImage from "../components/TestWithImage"

storiesOf("01-Demo|Folder/Component", module)
  .add("Base", () => {
    return <Button>Click</Button>
  })

  .add("Static GraphQL Image", () => {
    return <TestWithImage />
  })
